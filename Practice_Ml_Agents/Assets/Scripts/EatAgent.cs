﻿using Game.Net;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;
using UnityEngine.UI;

public enum Food
{
    chili_sauce,
    pepper_can,
    rock,
    sock,
}
public class EatAgent : Agent
{
    public int charID;
    public int Thirstynumber, peenumber , waternumber, foodindex;
    public float foodnumber;
    Slider thirsty,pee;
    Image water,food;
    public Sprite[] foods = new Sprite[11];
    public Sprite[] hotfoods = new Sprite[10];
    public Sprite[] pepperfoods = new Sprite[10];
    Text score;
    public Animator anim;
    public bool isaction, issweat, ispepper, iseatrock, issock, isjoke, isshock, isquillpen, islollipop, isspoon, ismosquito, issnack, gameover;
    public WaterBottle waterBottle;
    public AudioManager Audio;
    public EatAgent enemy;
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        base.Heuristic(actionsOut);
        ActionSegment<float> actionSegment = actionsOut.ContinuousActions;

        actionSegment[0] = Input.GetKeyDown(KeyCode.E).GetHashCode(); //吃東西
        actionSegment[1] = Input.GetKeyDown(KeyCode.D).GetHashCode(); //喝水
        actionSegment[2] = Input.GetKeyDown(KeyCode.Space).GetHashCode();//裝水
    }

    public override void Initialize()
    {
        base.Initialize();
        isaction = true;
        Thirstynumber = 0;
        peenumber = 0;
        waternumber = 100;
        foodnumber = 100;
        foodindex = 0;
        if (this.gameObject.name == "Prick")
        {
            charID = 1;
            thirsty = transform.parent.Find("PrickCanvas/Water/Slider").GetComponent<Slider>();
            pee = transform.parent.Find("PrickCanvas/Pee/Slider").GetComponent<Slider>();
            water = transform.parent.Find("PrickCanvas/Water/Water").GetComponent<Image>();
            food = transform.parent.Find("PrickCanvas/Food").GetComponent<Image>();
            food.sprite = foods[foodindex];
            score = transform.parent.Find("PrickCanvas/Prick/ScoreText").GetComponent<Text>();
            waterBottle = transform.parent.Find("Prick_waterbottle").GetComponent<WaterBottle>();
        }
        else if (this.gameObject.name == "Man")
        {
            charID = 2;
            thirsty = transform.parent.Find("ManCanvas/Water/Slider").GetComponent<Slider>();
            pee = transform.parent.Find("ManCanvas/Pee/Slider").GetComponent<Slider>();
            water = transform.parent.Find("ManCanvas/Water/Water").GetComponent<Image>();
            food = transform.parent.Find("ManCanvas/Food").GetComponent<Image>();
            food.sprite = foods[foodindex];
            score = transform.parent.Find("ManCanvas/Man/ScoreText").GetComponent<Text>();
            waterBottle = transform.parent.Find("Man_waterbottle").GetComponent<WaterBottle>();
        }
        anim = this.transform.GetComponent<Animator>();
        Audio = this.GetComponent<AudioManager>();
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        base.OnActionReceived(actions);
        float iseat = 0;
        float isdrink = 0;
        float isfillwater = 0;
        if (gameover == true)
        {
            return;
        }
        if (isaction == false && PhotonNetwork.IsMasterClient == true)
        {
            if (peenumber >= 100)
            {
                isaction = true;
                Anim("Pee");
                Debug.Log("Pee");
                return;
            }
            if (Thirstynumber < 100)
            {
                iseat = actions.ContinuousActions[0];
            }
            if (waternumber > 0 && Thirstynumber > 0)
            {
                isdrink = actions.ContinuousActions[1];
            }
            if (waternumber <= 0)
            {
                isfillwater = actions.ContinuousActions[2];
            }

            if (iseat == 1 && Thirstynumber < 100)
            {
                if (ispepper == true)
                {
                    isaction = true;
                    Anim("eatpepper");
                }
                else if (isspoon == true)
                {
                    isaction = true;
                    Anim("spoon");
                }
                else if (waterBottle.isvitamin == true)
                {
                    isaction = true;
                    Anim("quickEat");
                }
                else
                {
                    isaction = true;
                    Anim("Eat");
                }
                Debug.Log("Eat");
            }
            if (isdrink == 1)
            {
                if (waterBottle.iscola == true)
                {
                    isaction = true;
                    Anim("Drink_Soda");
                }
                else if (waterBottle.iswine == true)
                {
                    isaction = true;
                    Anim("Drink_Wine");
                }
                else if (waterBottle.islemon == true)
                {
                    isaction = true;
                    Anim("Drink_Lemon");
                }
                else if (waterBottle.isvitamin == true)
                {
                    isaction = true;
                    Anim("Drink_vitamin");
                }
                else if (waterBottle.islaxative == true)
                {
                    isaction = true;
                    Anim("eatlaxative");
                }            
                else
                {
                    isaction = true;
                    Anim("Drink");
                }
                Debug.Log("Drink");
            }
            if (isfillwater == 1)
            {
                AddReward(0.01f);
                isaction = true;
                Anim("FillWater");
                Debug.Log("Fill Water");
            }
        }     
        
        Refrash();
    }

    public override void OnEpisodeBegin()
    {
        base.OnEpisodeBegin();
        SetReward(0);
        score.text = "0";
        Thirstynumber = 0;
        peenumber = 0;
        waternumber = 100;
        foodnumber = 100;
        water.fillAmount = 1;
        foodindex = 0;
        food.sprite = foods[foodindex];
        food.fillAmount = 1;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        base.CollectObservations(sensor);
        sensor.AddObservation(food.fillAmount);
        sensor.AddObservation(thirsty.value);
        sensor.AddObservation(pee.value);
        sensor.AddObservation(water.fillAmount);
    }
    void FoodChange(int foodindex)
    {
        if (issweat == true)
        {
            food.sprite = hotfoods[foodindex];
        }
        else if (ispepper == true)
        {
            food.sprite = pepperfoods[foodindex];
        }
        else
        {
            food.sprite = foods[foodindex];
        }
    }
    void Refrash()
    {
        if (Thirstynumber > 100)
        {
            Thirstynumber = 100;
        }
        if (Thirstynumber < 0)
        {
            Thirstynumber = 0;
        }
        if (peenumber > 100)
        {
            peenumber = 100;
        }
        if (peenumber < 0)
        {
            peenumber = 0;
        }
        if (waternumber < 0)
        {
            waternumber = 0;
        }
        score.text = (foodnumber).ToString();
        #region 吃
        if (foodnumber > 90)
        {
            FoodChange(0);
        }
        else if (foodnumber > 80)
        {
            FoodChange(1);
        }
        else if (foodnumber > 70)
        {
            FoodChange(2);
        }
        else if (foodnumber > 60)
        {
            FoodChange(3);
        }
        else if (foodnumber > 50)
        {
            FoodChange(4);
        }
        else if (foodnumber > 40)
        {
            FoodChange(5);
        }
        else if (foodnumber > 30)
        {
            FoodChange(6);
        }
        else if (foodnumber > 20)
        {
            FoodChange(7);
        }
        else if (foodnumber > 10)
        {
            FoodChange(8);
        }
        else if (foodnumber > 5)
        {
            FoodChange(9);
        }
        if (foodnumber <= 0)
        {
            AddReward(0.1f);
            food.sprite = foods[10];
            isaction = true;
            Anim("win");
            enemy.isaction = true;
            enemy.Anim("lose");
            EndEpisode();
            enemy.EndEpisode();
        }
        thirsty.value = (float)Thirstynumber / 100;
        pee.value = (float)peenumber / 100;
        water.fillAmount = (float)waternumber / 100;
        #endregion
    }
    public void Anim(string animname)
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("shock"))
        {
            if (isshock == true)
            {
                return;
            }
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("quill pen"))
        {
            if (isquillpen == true)
            {
                return;
            }
        }
        string[] anim_name = { "Eat", "Drink", "FillWater", "Pee", "Sweat", "Drink_Soda", "Drink_Wine",
            "eatrock", "Hiccup", "Get_drunk", "Smell_socks", "Look_Joke", "Drink_Lemon", "eatlaxative",
            "eatpepper", "Drink_vitamin", "spoon", "mosquito", "quill pen", "Lollipop", "shock", "win", "quickEat", "snackEat", "lose" };
        for (int i = 0; i < anim_name.Length; i++)
        {
            if (animname == anim_name[i])
            {
                anim.SetBool(anim_name[i], true);
            }
            else
            {
                anim.SetBool(anim_name[i], false);
            }        
        }
    }
    public void SendServer(int food ,int thirstynumber, int peenumber, int waternumber, float foodnumber)
    {
        Debug.Log("發送伺服器");
        isaction = true;
        byte[] charid = BitConverter.GetBytes(charID);
        byte[] thirsty = BitConverter.GetBytes(thirstynumber);
        byte[] pee = BitConverter.GetBytes(peenumber);
        byte[] water = BitConverter.GetBytes(waternumber);
        byte[] foodnumbers = BitConverter.GetBytes(foodnumber);
        byte[] foods = BitConverter.GetBytes(food);
        byte[] data = new byte[24];
        Array.Copy(charid, 0, data, 0, 4);
        Array.Copy(thirsty, 0, data, 4, 4);
        Array.Copy(pee, 0, data, 8, 4);
        Array.Copy(water, 0, data, 12, 4);
        Array.Copy(foodnumbers, 0, data, 16, 4);
        Array.Copy(foods, 0, data, 20, 4);
        BufferFactory.CreateAndSendPackage(2, data);
    }
    private void Update()
    {
        if (isquillpen == true)
        {
            isaction = true;
            Anim("quill pen");
        }
        if (isshock == true)
        {
            isaction = true;
            Anim("shock");
        }
        if (ismosquito == true)
        {
            isaction = true;
            Anim("mosquito");
        }
    }
    public void PlayerAudios(PlayerAudio audio)
    {
        Audio.PlayerAudio(audio);
    }

    public void ReactAudios(ReactAudio audio)
    {
        Audio.ReactAudio(audio);
    }
    public void GameOver()
    {
        gameover = true;
    }
}
