﻿using Game.Net;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleListener : Singleton<BattleListener>
{
    Queue<byte[]> awaitActionHandle;
    Queue<byte[]> awaitLiquidHandle;
    //初始化的方法 監聽戰鬥的網路消息
    public void Init()
    {
        awaitActionHandle = new Queue<byte[]>();
        awaitLiquidHandle = new Queue<byte[]>();
        NetEvent.Instance.AddEventListener(2, HandleActionInputS2C);
        NetEvent.Instance.AddEventListener(3, HandleLiquidInputS2C);
    }
    //釋放的方法 移除監聽網路消息
    public void Release()
    {
        NetEvent.Instance.RemoveEventListener(2, HandleActionInputS2C);
        NetEvent.Instance.RemoveEventListener(3, HandleLiquidInputS2C);
        awaitActionHandle.Clear();
        awaitLiquidHandle.Clear();
    }
    byte[] ActionData;
    byte[] LiquidData;
    public void HandleActionInputS2C(BufferEntity response)
    {
        ActionData = new byte[24];
        ActionData = response.proto;
        awaitActionHandle.Enqueue(ActionData);
    }
    public void HandleLiquidInputS2C(BufferEntity response)
    {
        LiquidData = new byte[24];
        LiquidData = response.proto;
        awaitLiquidHandle.Enqueue(LiquidData);
    }
    //調用網路事件的方法
    public void PlayerAction(Action<byte[]> action)
    {
        if (action != null)
        {
            if (ActionData != null)
            {
                if (awaitActionHandle.Count > 0)
                {
                    action(awaitActionHandle.Dequeue());
                    Debug.Log("處理飯");
                }
            }
        }
    }
    public void PlayerLiquid(Action<byte[]> action)
    {
        if (action != null)
        {
            if (LiquidData != null)
            {
                if (awaitLiquidHandle.Count > 0)
                {
                    action(awaitLiquidHandle.Dequeue());
                    Debug.Log("處理水");
                }
            }
        }
    }
}
