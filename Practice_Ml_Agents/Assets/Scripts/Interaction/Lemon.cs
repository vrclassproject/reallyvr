﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lemon : InteractionObjectBase
{
    public WaterBottle waterBottle;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Prickwaterbottle")
        {
            waterBottle = GameObject.Find("EatArea/Prick_waterbottle").GetComponent<WaterBottle>();
        }
        else if (other.tag == "Manwaterbottle")
        {
            waterBottle = GameObject.Find("EatArea/Man_waterbottle").GetComponent<WaterBottle>();
        }
        if (waterBottle != null)
        {
            if (waterBottle.eatAgent.waternumber > 0 && waterBottle.isactive == false)
            {
                if (isfirst == false)
                {
                    Effect();
                    isfirst = true;
                }
            }
        }
    }
    public override void Effect()
    {
        //if (PhotonNetwork.IsMasterClient == true)
        //{
        //    waterBottle.state = Liquid.lemon.GetHashCode();
        //    waterBottle.SendServer(waterBottle.state, waterBottle.eatAgent.Thirstynumber, waterBottle.eatAgent.peenumber, waterBottle.eatAgent.waternumber);
        //}
        waterBottle.state = Liquid.lemon.GetHashCode();
        waterBottle.islemon = true;
        isactivate = false;
        this.gameObject.SetActive(false);
        //PhotonNetwork.Destroy(this.gameObject);
    }

    public override void SelectEnter()
    {
        base.SelectEnter();
    }
    public override void SelectOver()
    {
        base.SelectOver();
    }
}
