﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vitamin : InteractionObjectBase
{
    public WaterBottle waterBottle;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
        //if (ishold)
        //{
        //    if (this.transform.eulerAngles.x > 300)
        //    {
        //        isactivate = true;
        //    }
        //}
        if (this.transform.eulerAngles.x > 300)
        {
            isactivate = true;
        }
        else
        {
            isactivate = false;
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Prickwaterbottle")
        {
            waterBottle = GameObject.Find("EatArea/Prick_waterbottle").GetComponent<WaterBottle>();
        }
        else if (other.tag == "Manwaterbottle")
        {
            waterBottle = GameObject.Find("EatArea/Man_waterbottle").GetComponent<WaterBottle>();
        }
        if (waterBottle != null)
        {
            if (waterBottle.eatAgent.waternumber > 0 && waterBottle.isactive == false)
            {
                if (isfirst == false && isactivate == true)
                {
                    Effect();
                    isfirst = true;
                }
            }
        }
    }
    public override void Effect()
    {
        if (PhotonNetwork.IsMasterClient == true)
        {
            waterBottle.state = Liquid.vitmain.GetHashCode();
            waterBottle.SendServer(waterBottle.eatAgent.charID, waterBottle.state, waterBottle.eatAgent.Thirstynumber, waterBottle.eatAgent.peenumber, waterBottle.eatAgent.waternumber, waterBottle.eatAgent.foodnumber);
        }

        //waterBottle.isvitamin = true;
        Audio.ObjectAudio(ObjectAudio.vitamin);
        isactivate = false;
    }

    public override void SelectEnter()
    {
        base.SelectEnter();
        //ishold = true;
    }
    public override void SelectOver()
    {
        base.SelectOver();
        //isactivate = false;
        //ishold = false;
    }
}
